# __Re-Homologar atributos por fallas en atributos no homologados__

Este flujo es un flujo interno del pim que busca re-enviar mensajes a los fliujos patch o full product para re-homolar mensajes por problemas de homologacion, este flujo se divide en 3 partes :

1) ### __Descargar el archivo csv con el historico de errores__ : este proceso es un flujo que inicia en un enpoint "DownloadCsvReHomologarProduct" el cual retorna un csv que debe ser editado para poder subirlo.

```JSON
{
    "from":"2023-06-08",
    "until":"2023-06-09"
}
```

```mermaid
flowchart TD
    subgraph ZA["Download CSV with errors"]
    A[" get the history of logs with error en cloudwatch CLOUDWATCH_LOG_GROUP_ERROR_ATTRIBUTES_HOMOLOGATION "] -->
    B[" los ordena en el formato del csv esperado y responde con el archivo"]
    end 
```
   
2) ### __Subir el archivo csv para crear las homologaciones pendientes__ : este proceso es un flujo que inicia en un enpoint y su funcion es crear las homologaciones del csv, las columnas del csv son las siguientes: 

```JSON
    "id":"", ==> taskId
    "sku":"", ==> product SKUSeller
    "attributeName":"", ==> name of the attribute in Income Message
    "attributeValue":"", ==> value of the attribute in Income Message 
    "attributeType":"", ==> PRODUCT/VARIANT
    "attributeValueType":"" ==> TEXT/LIST
    "homologationCode":"" ==> product attribute in PIM to homologate the input to that attribute in PIM
    "homologationOption":"" ==> In case that the attribute is List it is the option attribute homologation
```

```mermaid
flowchart TD
    A[" Endpoint UploadCsvAttributesHomologation  "] 
    --> |header reHomologate optional| B(" homologateAttribute map file to JSON ARRAY AttributeCsvDto")
    --> C(" For each line of the file the flow check if the attributes exist 'product/variant' if does not exist it create it ")
```
   
3) ### __Re homologar los mensajes con errores__ : este paso es opcional y solo se ejecuta si en los header si envia el parametro "reHomologate" en true.

```mermaid
flowchart TD
    A{ reHomologate }
    A-->|false|C(" only create the attribute flow 2 ")
    A-->|true|B(" get the log from cloudwatch by Id ")
    B-->D{ rawMessage?.fileType }
    D-->|true|E(" Clean the hash in product table and send the sqs to the import Full product homologation ")
    D-->|false|F(" send the message to the Patch Homologation Flow ")
```

# Enviroments

# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas