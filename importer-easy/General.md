# cenco-pim-importer-easy

##  __Durante Migracion__

El importer tiene como funcion inicial bajar la data de forma controlada (usando un cron que cada cierto tiempo se trae un numero de productos) de las bases de datos de Vtext productivo y homologar esta informacion al Cenco-pim, esto se realiza por 2 flujos principales :

- **Bajada de toda la informacion desde vtext por medio del cron paginado** (G)  : este flujo se basa es un cron que de forma paginada baja los productos desde vtext, sube la info a un JSON en el bucket S3 y notifica a un consumer de importacion que se encarga de homologar esta informacion para enviarla al pim [ver mas](upload-products-to-s3-from-vtex-cronjob.md)
  
- **Bajada de informacion desde vtext de algunos productos especificos basado en VtextId** : este flujo se creo especificamente para productos cross marketplace pero la funcionalidad se basa en tomar los productos de la tabla "_products_mkp_" con estados pendiente, de esta tabla el flujo toma un numero de producto (50 por pasada valor en duro en codigo), este flujo comparte la funcionalidad del cron paginado donde baja la informacion del producto y envia al consumer de homologacion al PIM, y en paralelo se baja la data de precios de los productos y los homologa directo al PIM. [ver mas](get-product-from-vtext-mkp-cronjob.md)

##  __Posterior Migracion__

Posterior a la migracion incial se prentende dejar el proyeto como puerta de entrada al Cenco-PIM de tres flujos principales :

- **Actualizacion de informacion de productos desde PseudoPIM** : este flujo inicia en un topico especial al que PseudoPim envia los eventos de creacion o actualizacion de productos y variantes, este flujo se encarga de homologar al PIM esta informacion. [ver mas](products-with-change-from-vtex.md)
  
- **Actualizacion de precios desde PseudoPIM** (G)  : este flujo inicia en un topico especial al que PseudoPim envia los eventos de creacion o actualizacion de precios este flujo se encarga de homologar al PIM esta informacion. [ver mas](create-update-price-from-vtex.md)
  
- **Actualizacion de imagenes desde el gestor de medias** (G) : este flujo inicia en un endpoint al cual el gestor de medias envia las peticiones de creacion o actualizacion de imagenes, la informacion de las peticions es recibida y enviada a un consumer de homolacion de media al PIM. [ver mas](media-manager.md)

## __Re-Exportar Productos desde VTEX__

- Si se requiere traer toda la informacion actualizada desde vtex se puede utilizar el flujo (**Bajada de informacion desde vtext de algunos productos especificos basado en VtextId**) para esto solo es necesario agregar la al tabla "products-mkp" el "external_sku" del producto, "vtex_id_product" y status "Pending". De esta forma el cron los tomara en la siguiente pasada y rehomolgara el producto al pim (producto y precio), los cambios solo surtiran efecto si hay cambios en precios o productos vs lo que se guardo en el hash de precio/productos, opcionalmente si se requiere exportar sin importar si hay o no cambios se puede borrar los hash de la tabla producto/precios. [ver mas](get-product-from-vtext-mkp-cronjob.md)

- Otra posibilidad es utilizar el endpoint de re-export product from Vtex el cual toma la informacion en Vtex y lo envia al flujo **Bajada de informacion desde vtext de algunos productos especificos basado en VtextId** [ver mas][ver mas](re-enviar-products-importer-pim.md)

## __Re-Exportar Productos desde Importer__

- Este flujo toma el archivo de la UrlLastExported en la tabla de product y la reenvia al PIM. [ver mas](re-enviar-products-importer-pim.md)

## __Re-Exportar Productos desde PSEUDOPIM__

- Este flujo se basa en emular el flujo normal de recepcion de informacion desde la SNS de Pseudo-PIM, pero en lugar de iniciar en la SNS se basa es re-enviar mensajes almacenados en el grupo de logeo de CloudWatch "**Easy-Pseudo-Pim-Import-Product**" al flujo "**Actualizacion de informacion de productos desde PseudoPIM**", el tiempo desde que se re-envia el mensaje hasta que se homologa dependera de la cantidad de mensajes en la cola del consumer. [ver mas](re-enviar-mensajes-productos-pseudo-pim.md)

## __Re-Exportar Precios desde PSEUDOPIM__ (G)

- Este flujo se basa en emular el flujo normal de recepcion de informacion desde la SNS de Pseudo-PIM, pero en lugar de iniciar en la SNS se basa es re-enviar mensajes almacenados en el grupo de logeo de CloudWatch "**Easy-Pseudo-Pim-Import-Price**" al flujo "**Actualizacion de precios desde PseudoPIM**", el tiempo desde que se re-envia el mensaje hasta que se homologa dependera de la cantidad de mensajes en la cola del consumer. [ver mas](re-enviar-mensajes-precios-pseudo-pim.md)

## __Re-Homologar atributos por fallas en atributos no homologados__ (J)

- Este flujo se basa en crear una homologacion a nivel de la base de datos (si no existe) y posterior renviar los mensajes solicitados, el flujo en resumidas se basa en tomar mensajes del grupo de logeo de CloudWatch "**Easy-Error-Attributes-Homologation**" y reenviar el mensaje al flujo correspondiente ( enviara a **Actualizacion de informacion de productos desde PseudoPIM** si mensaje es patch o al flujo **Bajada de toda la informacion desde vtext por medio del cron paginado** si el error se genero en el flujo de import full product info) [ver mas](re-enviar-mensajes-error-attributes.md).

## __Re-Exportar Productos desde S3__ (G)

- Este flujo se basa en re-homologar la informacion traida desde vtex la ultima vez que paso el cron, este flujo toma el archivo json que se bajo la ultima vez que se corrio el cron paginado desde vtex y re-enviarlo al flujo **Bajada de toda la informacion desde vtext por medio del cron paginado** pero partiendo desde el consumer de homologacion, este flujo tiene como debilidad que puede no tener la informacion actualizada. [ver mas](re-enviar-mensajes-productos-s3-pim.md)
