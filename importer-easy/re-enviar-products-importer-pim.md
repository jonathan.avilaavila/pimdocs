# __Re-Exportar Productos desde Importer__ 

Este es un flujo corto que inicia con un enpoint post el cual recibe el siguiente mensaje: 

```JSON
{
    "country":"CL", ===> country
    "getFromVtext":false, 
    "vtexIdSku":[], ===> array of productsVtextId to export to PIM
    "sellerSku":[] ===> array of products sku to export to PIM
}
````
**Note:** Si el valor de "getFromVtext" es true se debe suministrar un arreglo de vtexIdSku, de los contrario se debe enviar un arreglo de sellerSku, en caso de que "getFromVtext" sea false se obviara el valor de vtexIdSku.

```mermaid
flowchart TD
    A["Endpoint Product reExportProduct"] -->
    C{"getFromVtext"}
    C -->|false| E["reExport for each sku in sellerSku invoke reExportToCountry"] 
    --> F["reExportToCountry find product in DB and Send sqs to PIM with url_last_exported"]
    C -->|true| D["reExportFromVtext"] 
    --> G["getVariantsFromVtex get all variantsVtextId by ProductVtextId "]
    --> H["getVariantsFromVtex  for each variantsVtextId map the info to InputRawProductDto"]
    --> I["Upload the JSON to S3 and send the SQS to the homologation consumer"]
```

# Enviroments

- [ ] AWS_SQS_SEND_PRODUCT_VARIANTS_TO_S3_FROM_VTEX : from cron to Consumer
- [ ] AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER / AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER_AR: from importer to PIM Easy-Consumer
- [ ] AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER / AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER_AR from importer to PIM Price-Unitary

# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas