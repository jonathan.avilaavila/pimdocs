# products-with-change-from-vtex

Este flujo resulta en un flujo critico que recibe los mensajes desde el Pseudo PIM y los mapea al DTO esperado por el PIM, este flujo maneja los siguientes tipos de eventos :

```js
{
  CREATE_GENERIC_PRODUCT = 'CREATE_GENERIC_PRODUCT',
  CREATE_VARIANT_FOR_PRODUCT = 'CREATE_VARIANT_FOR_PRODUCT',
  UPDATE_VARIANT_PROPERTIES = 'UPDATE_VARIANT_PROPERTIES',
  UPDATE_PRODUCT = 'UPDATE_PRODUCT',
  UPDATE_VARIANT = 'UPDATE_VARIANT',
  UPDATE_PRODUCT_ATTRIBUTES = 'UPDATE_PRODUCT_ATTRIBUTES',
  UPDATE_VARIANT_ATTRIBUTES = 'UPDATE_VARIANT_ATTRIBUTES',
  REXPORT_FROM_VTEXT = 'REXPORT_FROM_VTEXT',
}
```

```mermaid
flowchart TD
   subgraph ZA["consumer-cenco-pim-importer-easy-products-with-change-from"]
    A["validateMessage (validate if product has productId o skuId)"] --> 
    B["getPatchProduct map from the input message to PatchInputRawProductDto depending of the message input"] -->
    C["mapPatchToPim map from PatchInputRawProductDto to InputRawProductDto and send it to PIM"] -->
    D["updateFullProduct upload file to s3 and update to the url in product table"] -->
    E["exportPatchToCountry send sqs to stg-cl-consumer-cenco-pim-service-easy-importer-patch-prod"] 
   end
```

# Enviroments

- [ ] AWS_SQS_MIGRATE_PRODUCT_EASY_FARGATE
- [ ] STG_CL_AWS_SQS_UPDATE_PATCH_PRODUCT / STG_AR_AWS_SQS_UPDATE_PATCH_PRODUCT


# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas