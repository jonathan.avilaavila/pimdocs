# __Re-Exportar Productos desde PSEUDOPIM__

Este flujo inicia con un enpoint el cual recibe el siguiente mensaje, y busca los logs en el grupo de logeo "CLOUDWATCH_LOG_GROUP_PRODUCT", donde el LogStream es el product skuSeller

```JSON
{
    "timestamp":"2023-05-22T12:40:49.406-04:00", ==> fecha del mensaje a re-exportar
    "vtextId":"1314049001" ==> este campo es el logStream a exportar en este caso debe ser el product !!!External SKU!!!
}
```

```mermaid
flowchart TD
    A[" Endpoint ReExportProductFromSeudoPim recive the message"] -->
    B[" get the logs from cloudwatch Log group and filter by timeStamp "] -->
    C[" in the menssage has the field inputData it send the json value in  inputData to consumer in importer of homolgate patch product"] 
```

# Enviroments

- [ ] AWS_SQS_MIGRATE_PRODUCT_EASY_FARGATE
- [ ] AWS_SQS_PRODUCT_FROM_SEUDO_PIM
- [ ] STG_CL_AWS_SQS_UPDATE_PATCH_PRODUCT / STG_AR_AWS_SQS_UPDATE_PATCH_PRODUCT


# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas