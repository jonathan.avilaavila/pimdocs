# get-product-from-vtext-mkp-cronjob

Este flujo se basa en tomar productos desde la tabla "product-mkp" (cada pasada del cron "get-product-from-vtext-mkp-cronjob" toma 50 productos en status "pending") y los enviar al consumer "consumer-cenco-pim-importer-easy-import-product" para ser homologado al pim.

**Note:** Este flujo permite a nivel del PIM crear producto con un SKU PIM qeu inicie en MK para ellos es necesario en la columna forcedSkuPrefix with MKP.

```mermaid
flowchart TD
   subgraph ZA["Cron"]
    A["getProductList (using vtex_id_product get the vtext_variants_id)"] --> 
    B["getVariants (using vtext_variants_id get the product Info and map de requiere fields)"] -->
    C["saveVariantsToS3 (Upload the products mapped before to S3 and send it to the Flow of Homologate Product)"] -->
    D["getPricesForProductAndHomologateToPim (using vtext_variants_id get the prices from vtext and homologateit)"]
   end

   subgraph ZB["Consumer Homologate Product"]
    PA["validateMessage (validate that the income sqs has attributes sellerId, urlCsvOut)"] --> 
    PB["downloadAndValidateFromS3 (download s3 JSON and map it to InputRawProductDto to homologate it)"] -->
    PC["updateUrlAndDateProduct (that step allow to update the url in the product table to be able to re-homologate from this point)"] -->
    PD["homologateProduct (homologate the product from InputRawProductDto to PIM DTO and send the SQS to PIM)"] 
   end

   subgraph ZC["Consumer Homologate price (este fl)"]
    PEA["removeUnchanged (compare hash from the table with the hash of the recived mapped data)"] --> 
    PEB["validateAndmapToPim (map to CreatePricesDto and send the sqs to the unitary prices consumer in PIM)"]
   end

   ZA --> ZB
   ZA --> ZC
```

# Enviroments

- [ ] AWS_SQS_SEND_PRODUCT_VARIANTS_TO_S3_FROM_VTEX : from cron to Consumer
- [ ] AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER / AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER_AR: from importer to PIM Easy-Consumer
- [ ] AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER / AWS_SQS_TOPIC_UPDATE_PRODUCT_IMPORTER_AR from importer to PIM Price-Unitary

# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas