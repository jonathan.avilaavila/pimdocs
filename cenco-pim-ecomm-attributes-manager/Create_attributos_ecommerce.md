# Create_attributos_ecommerce

Este flujo se basa en un endpoint que recibe la informacion para crear un atributo con un valor generico/default para el chanel, el cuerpo del mensajes enviado por post es :

```json
{
    "attributeName":"test5",
    "defaultValue":"5",
    "channelId":"d4c9e3da-f088-11eb-84db-0242ac110002",
    "type":"PRODUCT"
}