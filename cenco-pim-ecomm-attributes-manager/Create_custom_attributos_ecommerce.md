# Create_custom_attributos_ecommerce

Este flujo inicia con archivo csv cargado a nivel de front este archivo pasa por el bff y llega al EAM donde se sube el archivo al S3 y se envia un sqs al consumer de split de csv a unitario, luego el unitario splitea el archivo y envia linea por linea al consumer principal que creara el atributo custom y luego se le pide al PIM que envie el rawValue del producto (re-exporte) al consumer base de agregar atributos para productos, el flujo es:

```mermaid
flowchart TD

subgraph ZA["Create Custom Attribute for Products"]
direction LR
  A(Upload CSV by front) --> B(BFF pass to EAM)
  B --> C(EAM endpoint send  Msg to SQS split)
end

subgraph ZB["EAM Internal Flow"]
    D(consumer-cenco-pim-ecomm-attrib-manager-split-csv)-->E
    E(consumer-cenco-pim-ecomm-attrib-manager-create-update)-->F(PIM consumer export-product-to-eam)
end

subgraph ZC[" "]
    G(export-product-to-eam recive chanelId and Product SKU Seller)-->H
    H(Get the Rawvalue upload to S3 and send it to )-->I(Normal flow of consumer-cenco-pim-ecomm-attrib-manager-aggregate-upload)
end

ZA --> ZB --> ZC
```

# Proceso de Deploy


# Enviroments

- FRONT
  - Endpoint : ""
- BFF
   - HOST_EAM_AR : ""
   - HOST_EAM_BR : ""
   - HOST_EAM_CO : ""
   - HOST_EAM_CL : ""
   - HOST_EAM_PE : ""
- EAM
   - productCreated : ""
   - productAttributeCreated : ""
   - splitCsvAttribute : ""
   - reExportProductPimEam : ""
- PIM
   - SQS_RE_EXPORT_PRODUCT_PIM_EAM : ""
   - SQS_CREATE_UPDATE_ATTRIBUTE_PRODUCT_EAM : ""

# Como monitorear el flujo

# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas

> <details>
>   <summary style="background-color: red; color:yellow"> Valor de attributos quedo validado a solo numerico  </summary>
>   Durantes las pruebas se logro observar que cuando se carga un valor custom para un SKU especifico el mismo podia inicialmente ser de cualquier tipo (string,numero, con caracteres especiales) pero como al momento del desarrollo solo fue necesario el CartLimit se dejo una validacion que solo cree valores numericos, queda pendiente crear un sistema de validacion y/o tipos de atributos en caso de requerirse en un futuro.
> </details>

.
# Plan de Pruebas

# Historial de Cambios

> <details>
>   <summary style="background-color: lightblue; color:black"> Eliminar atributos customs  </summary>
>   Si el valor que llega para un SKU es vacio "" se eliminara de la lista de valores custom para productos y se seguira el flujo de re-exportacion para agregar valores genericos.
> </details>