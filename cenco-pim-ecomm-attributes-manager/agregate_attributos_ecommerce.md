# Agregate_attributos_ecommerce

Este el flujo principal se basa en recibir desde el PIM los productos actualizados y agregarle los atributos creados para el channel, este flujo puede agregar el valor default que esta en la tabla **attribute_eccommerce** o agregar un valor especifico para el sku del producto en el channel del productos si existe en la tabla **product_attribute_eccommerce**

El flujo inicial en un **Consumer** : **consumer-cenco-pim-ecomm-attrib-manager-aggregate-upload**  que recibe el mensaje del pim con la URL del json (donde se encuentra el rawvalue del producto)

```mermaid
flowchart  TD
A[PIM SNS PRODUCT UPDATED]
-->  B(SQS sqs_product_attribute_manager_ar)
A1[PIM Consumer export-product-to-eam]  -->B
B  -->  C(consumer-cenco-pim-ecomm-attrib-manager-aggregate-upload)
C  -->  D(downloadFileFromS3)
D  -->  E(addAttributeEccomerce)
E  -->  F(add default value)
F  -->  H(uploadChangesToS3)
E  -->  G(add custom value)
G  -->  H
H  -->  I(notifyMessage SNS AR-PRODUCT-UPDATED-EAM)
I-->J(Exporter)
```

# Proceso de Deploy

- [ ] Crear la cola que escucha el consumer "sqs_product_attribute_manager_ar"
- [ ] Subcribir la cola a la SNS del PIM "STAGING-AR-PRODUCT-UPDATED"
- [ ] Crer la SNS "AR-PRODUCT-UPDATED-EAM"
- [ ] Crear las variables de entornos en el repo de Gitlab
- [ ] Desplegar el repositorio en K8s
- [ ] Notificar a exporter para que se subcriba a la SNS de salida "AR-PRODUCT-UPDATED-EAM"

# Enviroments

- [ ] SQS_PRODUCT_CREATED_ATTRIBUTE_MANAGER 
- [ ] AWS_SNS_TOPIC_UPDATE_PRODUCT

# Como monitorear el flujo

# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas

> <details>
>   <summary style="background-color: red; color:yellow"> (!) Atributos eccomerce para variantes   </summary>
>   Es necesario refinar como trabajarian attributos eccomerce para variantes por ahora solo se ha desarrollado para porudctos aun cuando se penso agregar el campo "type" en las tablas de atributos con valor generico y en la tabla de valores custom no se probo ni se considero en los flujos.
> </details>

.
# Plan de Pruebas


# Historial de Cambios