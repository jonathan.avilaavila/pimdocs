# # cenco-pim-ecomm-attributes-manager

El proyecto tiene como fundamento de negocio agregar atributos ecommerces a los productos (estos son atributos que no pertencen al producto sino que permiten manipular como el mismo es indexado, vendido, o ubicado, ejemplo :

- **Cart-Limit**  : se refiere a la cantidad maxima de productos a ser vendidos por pedido.

# Flujos

El proyecto actualmente cuenta con 3 flujos principales :

- **Agregar atributos eccommerce a productos** : este el flujo principal se basa en recibir desde el PIM los productos actualizados y agregarle los atributos creados para el channel, este flujo puede agregar el valor default que esta en la tabla **attribute_eccommerce** o agregar un valor especifico para el sku del producto en el channel del productos si existe en la tabla **product_attribute_eccommerce** [ver mas](agregate_attributos_ecommerce.md)

- **Create attributos ecommerce**  :  este flujo se basa en crear atributos eccommerce para un channel especifico. [ver mas](Create_attributos_ecommerce.md)

- **Crear valores custom para sku específicos** : este flujo habilita la posibilidad de agregar valores custom a un sku y un channel a los atributos ecommerce existentes.  [ver mas](Create_custom_attributos_ecommerce.md)


# Variables de entonos del proyecto 

- **COUNTRY**
- **AWS_BUCKET**
- **SQS_SPLIT_FILE_TO_UNITARY**
-  **SQS_CREATE_UPDATE_ATTRIBUTE_PRODUCT**
- **SQS_PRODUCT_CREATED_ATTRIBUTE_MANAGER**
-  **SQS_RE_EXPORT_PRODUCT_PIM_EAM**
-  **AWS_SNS_TOPIC_UPDATE_PRODUCT**
-  **FULL_PERMISSIONS_ROLE**