# Propagate prices producto cross multi-channel

El flujo tiene funcionalidad replicar los precios del channel owner en los demas canales a los cuales tenga definido en la tabla de configuración luego de cualquier evento que pase por el priceService v1 y v2

```mermaid
flowchart TD
    subgraph ZA["Endpoint convert product multi channel"]
    direction LR
    A("Upload csv convert product cross") --> B("take configuration in DB") --> C("converted product cross multi channel")
    end

    subgraph ZB ["PIM Endpoint Flow"]
    D("Download in front excel prices where product owner to edit prices") --> E("edit prices and upload file") --> F("this replicate prices in other channel definited in configuration for channel owner")
    end

ZA --> ZB
```
# Plan de Pruebas

>  <details>
>   <summary style="background-color: lightgreen; color:black"> Click button download excel prices for product  </summary>
>  estar autenticado en el front con acceso a ver el boton de descarga, y esperar a recibir una notificación y un correo donde vendan los excel con los precios de los productos solicitados dependiendo su brand en la peticion la cual debe estar en los productos de elastic en el array de channel en la posicion 0 como principal, al volver a subir los precios si en la configuración de la BD existe que este propage los precios a un channel especifico, en el excel solo vera la pestaña del channel principal para editar y al subir y ver del front estos precios se veran en ese channel y en los channel cuales tiene definido en BD
> </details>

# Enviroments

# Database
