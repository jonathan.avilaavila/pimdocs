# Download Cross Products Multi Channel

El flujo tiene como funcionalidad disponibilizar la usuario la posibilidad de descargar uno o varios [csv](export-product-csv-[numeroArchivo].csv) con la asignación de los productos a múltiples canales. esto entrega una notificación al front con los links y ademas un correo con los link para poder descargar los archivos.

```mermaid
flowchart TD
    subgraph ZA["Front To PIM"]
    direction LR
    A("Front Download CSV") --> B("Bff recived petition with brand, sellerid, country and authorization and pass to PIM") --> C("PIM product-export/download-csv-multiple-channel")
    end

    subgraph ZB ["PIM Endpoint Flow"]
    D("Endpoint recive petition with headers and validate it") --> E("Create the task type CSV_EXPORT_PRODUCT ") --> F("Send it to the consumer by sqs AWS_SQS_TOPIC_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL")
    end

    subgraph ZC ["Consumer internal Flow"]
    G("Update the task start process") --> H("get configuration from table with type CSV_EXPORT_PRODUCT_MULTI_CHANNEL") --> I("get headersCsv, maxLineasPorArchivo and elementosPorIteracion")
    I --> J("with this get from elastic count product where principal brand it's in position 0 in channel raw") --> K("each records by iteration in array to max size by csv and create")
    K -->L("upload csv to S3") -->M("updaste task with link from files uploaded in S3")--> N("send notification front with links files created and send mail") -->S("Update Task Process Status") --> R("Update Task Errors") --> Q("Update Task Finish and duration and finish")
    end

ZA --> ZB --> ZC
```

# Plan de Pruebas

>  <details>
>   <summary style="background-color: lightgreen; color:black"> Click button download csv product cross  </summary>
>  estar autenticado en el front con acceso a ver el boton de descarga, y esperar a recibir una notificación y un correo donde vendan los csv con o sin productos dependiendo su brand en la peticion la cual debe estar en los productos de elastic en el array de channel en la posicion 0 como principal
> </details>

# Enviroments

- [ ] AWS_SQS_TOPIC_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL
- [ ] REPLICAS_CONSUMER_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL=2
- [ ] MAX_REPLICAS_CONSUMER_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL=2
- [ ] RESOURCES_LIMIT_CPU_CONSUMER_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL=2
- [ ] RESOURCES_LIMIT_MEM_CONSUMER_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL=1000Mi
- [ ] RESOURCES_REQ_CPU_CONSUMER_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL=0.4
- [ ] RESOURCES_REQ_MEM_CONSUMER_EXPORT_CSV_PRODUCT_FOR_SEND_EMAIL=650Mi

# Database

``` sql
INSERT INTO configuration (id,created_at,update_at,code,json_config) VALUES
	 ('d1330005-fe5e-11ed-89f8-0e7ffa6ca5b9','2023-05-29 20:24:30','2023-05-29 21:06:30.731698','CSV_EXPORT_PRODUCT_MULTI_CHANNEL','{"headersCsv": [{"name": "sku", "title": "Sku"}, {"name": "originalChannel", "title": "OriginalChannel"}, {"name": "otherChannels", "title": "OtherChannels"}], "maxLineasPorArchivo": 10000, "elementosPorIteracion": 1000}');
```