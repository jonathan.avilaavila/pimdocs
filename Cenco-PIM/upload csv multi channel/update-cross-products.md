# Update Chanel Cross Products

El flujo tiene como funcionalidad disponibilizar la usuario la posibilidad de cargar un [csv](update-cross-products.csv) con la asignación de los productos a múltiples canales. El flujo funciona en **modo replace** es decir la lista de canales que se envia por producto reemplazara la existente, incluso hasta dejarlo solo con el canal original, el cual nunca puede ser cambiado por este flujo.

```mermaid
flowchart TD
    subgraph ZA["Front To PIM"]
    direction LR
    A("Front Uload CSV") --> B("Bff recived the file and pass to PIM") --> C("PIM product-imports/csv-cross-products")
    end

    subgraph ZB ["PIM Endpoint Flow"]
    D("Endpoint recive the file without validate it") --> E("Create the task type CSV_UPDATE_CROSS_PRODUCT ") --> F("Send it to the consumer by sqs AWS_SQS_TOPIC_UPDATE_CROSS_PRODUCTS")
    end

    subgraph ZC ["Consumer internal Flow"]
    G("Update the task status to IN_PROCESS") --> H("download from S3 and parse it to JSON") --> I("validate line by line and add to sort array")
    I --> J("Create an Entity Manager for all the lines") --> K("For each line update the channel field and in other pettition the raw value by json_merge_patch")
    K -->L("For each line Notify front in Product Updated SNS") -->M("For each line Update Elastic Document")--> N("For each line if producto include MKP send message to approval consumer to approve the product") -->S("Update Task Process Status") --> R("Update Task Errors") --> Q("Update Task Finish and duration and finish")
    end

ZA --> ZB --> ZC
```


# Plan de Pruebas

>  <details>
>   <summary style="background-color: lightgreen; color:black"> Agregar channel adicionales al producto  </summary>
>  Subir un CSV con varios productos donde por linea se agrege el producto tenga almenos 3 columna (SKU;CANALORIGEN;CANAL1), los productos pueden tener algunos 1 channel adicional y otros 2/3, el csv debe estar separado por ";" y reespetar las mayusculas en la cabecera.
> El resultado debe ser que al culminar la task el producto debe tener la db en el campo Channel la cantidad de canales del csv (considerar que el canal original no debe cambiar del puesto 0 del array)
> </details>
.
>   <details>
>   <summary style="background-color: lightgreen; color:black"> Quitar channel adicionales al producto  </summary>
>  Subir un CSV con varios productos donde por linea se agrege el producto tenga almenos 3 columna (SKU;CANALORIGEN;CANAL1), los productos pueden tener solo el canal original o tener 1 canal si antes tenian 2 
> El resultado debe ser que al culminar la task el producto debe tener la db en el campo Channel la cantidad de canales del csv (considerar que el canal original no debe cambiar del puesto 0 del array y no se puede eliminar)
> </details>
.
>  <details>
>   <summary style="background-color: red; color:yellow"> (!) Canal que no existe </summary>
>   Subir un archivo donde se intente agregar un canal que no exista en la DB o este mal escrito.
>   El resultado que al producto no se le agrega el canal con error si se intentaban agregar otros canales en la misma linea se agregaran todos los que existan y alertara con el mensaje de error " El channel ${r[c]} no existe y se intento agregar al producto  ${r['SKU']} para el sellerId ${this.payload.sellerId} " los que fallaron por estar mal escritos o no existir
> </details>

.
# Enviroments

- [ ] AWS_SQS_TOPIC_UPDATE_CROSS_PRODUCTS
- [ ] REPLICAS_CONSUMER_UPLOAD_CSV_PRODUCT_CROSS_PRODUCT=2
- [ ] RESOURCES_LIMIT_CPU_CONSUMER_UPLOAD_CSV_PRODUCT_CROSS_PRODUCT=2
- [ ] RESOURCES_LIMIT_MEM_CONSUMER_UPLOAD_CSV_PRODUCT_CROSS_PRODUCT=1000Mi
- [ ] RESOURCES_REQ_CPU_CONSUMER_UPLOAD_CSV_PRODUCT_CROSS_PRODUCT=0.4
- [ ] RESOURCES_REQ_MEM_CONSUMER_UPLOAD_CSV_PRODUCT_CROSS_PRODUCT=650Mi

.

# Proceso de Deploy

 1) Deplogear el bff con sus variables
 2) Deplogear el PIM con sus variables de entorno

.

# Deuda tecnica, mejoras, cambios pendientes, Errores frecuentes o fallas
.
>  <details>
>   <summary style="background-color: red; color:yellow"> (!) Exceso de peticiones del Task Services </summary>
>   En los procesos de finalizacion de la task se hacen 3 peticiones una para Actualizar el processStatus otras para status/errores y otra para el tiempo de duracion de la task.
> </details>
.
>  <details>
>   <summary style="background-color: orange; color:black"> Si falla una linea al estar todo dentro de un entity manager fallara todo el archivo </summary>
>   Ya que se actualizan 2 campos sencibles de la tabla de productos el channel y el rawvalue que son 2 json y adicionalmente la notificacion al topico SNS, si falla alguna linea del archivo por un error no controlado eso haria fallar todo el flujo de forma controlada pero el archivo quedaria en error.
> </details>
.
# Historial de Cambios

- **PM-5689** : Creacion del flujo inicial.